/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package runtime_polymorphism;

/**
 *
 * @author usama
 */
public class Runtime_Polymorphism {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Home home1=new Home();
        home1.muazzam_relation();
        
        Office office1=new Office();
        office1.muazzam_relation();
        
        Ground ground1=new Ground();
        ground1.muazzam_relation();
    }
    
}
